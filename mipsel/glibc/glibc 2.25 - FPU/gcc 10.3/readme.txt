:::::::::::::build_pc = x86_64 :::::::::::::::

linux 3.2.101
glibc 2.26
gcc 10.3.0
gdb 9.2
binutils 2.35.1

with-float: hard
with-arch: mips32
with-abi: 32
with-mips-plt

openssl none
libusb none
pcsc none